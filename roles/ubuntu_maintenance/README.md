# `ubuntu_maintenance` Ansible Role

The `ubuntu_maintenance` Ansible role manages essential maintenance tasks for Ubuntu systems, ensuring the system is updated, required packages are installed, unwanted packages are removed, and automatic security updates are configured.

## Requirements

- Target nodes should be running an Ubuntu distribution.
- SSH access to the target nodes.
- `sudo` privileges on the target nodes.
  
## Role Variables

- **essential_packages** (default: `curl`, `jq`, `neovim`, `htop`, `git`, `net-tools`, `neofetch`, `docker.io`, `qemu-guest-agent`): A list of packages that should be installed on the system.

- **unwanted_packages** (default: `apache2`): A list of packages that should be removed from the system.

- **Unattended-Upgrade** configuration options: These variables determine the behavior of the `unattended-upgrades` package, such as which packages are allowed or blacklisted for automatic updates, email notifications settings, unused packages removal, and auto-reboot settings.

## Dependencies

No external dependencies.

## Handlers

- **Restart unattended-upgrades**: Restarts the `unattended-upgrades` service after updating its configuration.

## Example Playbook

```yaml
- name: Ubuntu Maintenance
  hosts: bootstrap, master_nodes, worker_nodes
  become: yes
  roles:
    - roles/ubuntu_maintenance
```

## Tasks Overview

1. **Ensure system packages are updated**: This task updates all system packages.
2. **Remove unnecessary packages**: It will run `autoremove` to clean up unnecessary packages.
3. **Install essential packages**: Installs all the packages listed in `essential_packages`.
4. **Remove unwanted packages**: Removes packages listed in `unwanted_packages`.
5. **Ensure unattended-upgrades is installed**: Installs the `unattended-upgrades` package.
6. **Configure automatic security updates**: Places the configuration file for `unattended-upgrades` in the correct directory and ensures it has the correct permissions.

