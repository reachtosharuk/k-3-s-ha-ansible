# k3s_worker Ansible Role

This Ansible role is designated for setting up the worker nodes in a K3s cluster. It guarantees the installation of the specified K3s version and sets it up based on the provided variables.

## Requirements

- Ansible version 2.1 or newer.
- SSH access to the target worker nodes.
- `curl` needs to be installed on the target nodes.
- Worker nodes should have access to `https://get.k3s.io`.

## Role Variables

Variables are taken from the inventory's `vault.yml` and `group_vars/main-cluster.yml`.

**Main Variables**:
- `k3s_version`: The version of K3s you want to install.
- `bootstrap_node_ip`: The IP address of the bootstrap node.

**Sensitive Data (from `vault.yml`)**:
- `ansible_ssh_private_key`: The private SSH key for the Ansible user.
- `k3s_node_token`: Token used to join worker nodes to the cluster.

## Dependencies

None.

## Example Playbook

```yaml
- name: Setup k3s worker nodes
  hosts: worker_nodes
  become: yes
  gather_facts: no
  vars_files:
    - ../inventory/group_vars/main-cluster.yml
    - ../inventory/vault.yml
  roles:
    - roles/k3s_worker
```

**Pre-tasks**:

These tasks are executed before the main role:

- Create a temporary SSH key file on the Ansible controlling machine.
- Set the `ansible_ssh_private_key_file` variable to point to this temporary key.

**Post-tasks**:

After the main tasks of the role:

- Ensure the removal of the temporary SSH key file from the Ansible controlling machine.

## License

GPL-2.0-or-later (or specify your preferred license)

## Author Information

This role was created in 2023 by Brock Henrie, Spakl.

