# `k3s_bootstrap` Role

This role handles the setup and initialization of the K3s master node with specific configurations. 

## Tasks

1. **Check if k3s is already installed**: Using `systemctl`, this task checks if K3s is active. If K3s is not found or is inactive, further tasks will continue.
   
2. **Debug k3s check**: Outputs the status of the K3s check.

3. **Install k3s master node with configurations**: Installs K3s using the script from `https://get.k3s.io`. Configuration details include:
   
   - Specified K3s version using `k3s_version`
   - Cluster initialization
   - Disabling of `servicelb` and `traefik`
   - `tls-san` addresses specification
   - Various Kube configurations
   
   **Note**: This task runs only if K3s is inactive or not installed.

4. **Retrieve k3s node token**: Grabs the node token for joining worker nodes to the cluster. 

5. **Fetch k3s kubeconfig**: Acquires the kubeconfig, vital for `kubectl` connection to the K3s cluster.

6. **Store node token and kubeconfig**: Keeps the node token and kubeconfig as Ansible facts for other tasks or roles.

## Variables

- `k3s_version`: The desired version of K3s.
- `kube_vip_ip`: The Virtual IP for Kubernetes.
- `kube_vip_sans_names`: List of Subject Alternative Names for the Kubernetes API server certificate.

These variables should be defined in `inventory/group_vars/main-cluster.yml`.

## Playbook Example

This playbook sets up the K3s Bootstrap Node:

```yaml
---
- name: Setup k3s Bootstrap Node
  hosts: bootstrap
  become: yes  # Run tasks as sudo
  gather_facts: no  # Disable fact gathering
  vars_files:
    - ../inventory/group_vars/main-cluster.yml
    - ../inventory/vault.yml

  pre_tasks:
    - name: Create temporary SSH key file
      # ... [rest of the task]
      
    - name: Set ansible_ssh_private_key_file variable
      # ... [rest of the task]
      
  roles:
    - roles/k3s_bootstrap

  post_tasks:
    - name: Print k3s node token
      # ... [rest of the task]

    - name: Print k3s kubeconfig
      # ... [rest of the task]

    - name: Remove temporary SSH key file
      # ... [rest of the task]
```

When using this playbook, ensure that the required variables are properly defined in the `main-cluster.yml` and `vault.yml` within the inventory directory.

## Inventory Configuration

Your inventory configuration (`hosts.yml`) should define the `bootstrap` group with the appropriate hosts. For instance:

```yaml
bootstrap:
  hosts:
    master_node_boot:
      ansible_host: 10.10.4.60
```

