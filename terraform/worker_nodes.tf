
resource "proxmox_vm_qemu" "worker_node_0" {
  vmid        = 200
  name        = "worker-node-0"
  tags        = "k3s,worker"
  target_node = "proxmox"
  desc        = "worker k3s node"
  os_type     = "cloud-init"
  ciuser      = var.user
  cipassword  = var.password
  sshkeys     = file("../inventory/public_keys")
  clone       = "ubuntu-cloud"  # This is the VM/template ID you want to clone
  onboot      = true
  agent       = local.agent
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 4096
  balloon     = 1024
  disk {
    backup             = 0
    cache              = "none"
    format             = "raw"
    size               = "80G"
    storage            = "local-lvm"
    type               = "scsi"
  }
  network {
    bridge = "vmbr0"
    model  = "virtio"
    tag    = "4"
  }
  ipconfig0  = "ip=10.10.4.80/24,gw=10.10.4.1"
  nameserver = "10.10.4.1"
}
