terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9"
    }
  }
  backend "http" {
    
  }
}

provider "proxmox" {
  pm_api_url      = "https://10.10.1.2:8006/api2/json"
  pm_user         = "root@pam"
  pm_password     = "$$B@ckspace1!!"
  pm_tls_insecure = true  # Set to false if you have a valid SSL certificate
}
variable "user" {
  type = string
  description = "The username for the cloud-init user on the Proxmox virtual machine"
}

variable "password" {
  type      = string
  sensitive = true
  description = "The password for the cloud-init user on the Proxmox virtual machine"
}

locals {
  agent = 0
}